package com.duzieblo.kaloryk.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.duzieblo.kaloryk.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Damian on 23.10.2016.
 */
public class OverwiewCursorAdapter extends CursorAdapter {


    public OverwiewCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.overwiew_custom_list, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder holder = null;

        //if (view == null) {
            //holder = new ViewHolder();
        TextView tvMealsDate = (TextView) view.findViewById(R.id.tvMealsDate);
        TextView tvMealsNameCustomList = (TextView) view.findViewById(R.id.tvMealsNameCustomList);
        TextView tvTotalCalCustomList = (TextView) view.findViewById(R.id.tvTotalCalCustomList);

        //    view.setTag(holder);
        //} else {
        //    holder = (ViewHolder) view.getTag();
        //}



        String dateString = cursor.getString(cursor.getColumnIndexOrThrow("cal_add_date"));
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = format.parse(dateString);
            dateString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String meals = cursor.getString(cursor.getColumnIndexOrThrow("cal_meal_name"));
        String calories = cursor.getString(cursor.getColumnIndexOrThrow("cal_numb_of_calories"));

        tvMealsDate.setText(dateString);
        tvMealsNameCustomList.setText(meals);
        tvTotalCalCustomList.setText(calories + " " + context.getResources().getString(R.string.unit_calories));
    }

    private class ViewHolder {
        TextView tvMealsDate;
        TextView tvMealsNameCustomList;
        TextView tvTotalCalCustomList;
    }
}
