package com.duzieblo.kaloryk;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.duzieblo.kaloryk.fragments.KalAddFragment;
import com.duzieblo.kaloryk.fragments.KalMainFragment;
import com.duzieblo.kaloryk.fragments.KalOverwiewFragment;

public class KalMainActivity extends Activity implements KalMainFragment.KalMainFragmentListener {

    private String[] menus;
    private ListView lvDrawer;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    private static String _FRAGMENT_TAG = "KAL_FRAGMENT";
    private static String _MENU_POSITION = "MENU_POSITION";

    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kal_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerToggle = new ActionBarDrawerToggle(this,
                                                        drawerLayout,
                                                        R.string.draw_open,
                                                        R.string.draw_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);


        menus = getResources().getStringArray(R.array.list_menu);
        lvDrawer = (ListView) findViewById(R.id.lvDrawer);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                                                                android.R.layout.simple_list_item_1,
                                                                menus);
        lvDrawer.setAdapter(adapter);
        lvDrawer.setOnItemClickListener(getMenuListListener());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayShowHomeEnabled(true);
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(_MENU_POSITION);
            setActionBarTitle(currentPosition);
        } else {
            selectItem(0);
        }

        getFragmentManager().addOnBackStackChangedListener(new OnBackFragment());

    }

    private ListView.OnItemClickListener getMenuListListener() {
        ListView.OnItemClickListener onItemClickListener = new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        };
        return onItemClickListener;
    }

    private void selectItem(int position) {
        currentPosition = position;
        Fragment fragment;
        switch (position) {

            case 1:
                fragment = new KalAddFragment();
                break;
            case 2:
                fragment = new KalOverwiewFragment();
                break;
            default:
                fragment = new KalMainFragment();
                break;
        }
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment, _FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();

        setActionBarTitle(position);
        drawerLayout.closeDrawer(lvDrawer);
    }

    private void setActionBarTitle(int position) {
        String title;
        if (position == 0) {
            title = getResources().getString(R.string.app_name);
        } else {
            title = menus[position];
        }
        getActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.add_calories:
                selectItem(1);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClickAddCalories() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, new KalAddFragment(), _FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (getFragmentManager().findFragmentByTag(_FRAGMENT_TAG) instanceof KalAddFragment) {
            menu.findItem(R.id.add_calories).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private class OnBackFragment implements FragmentManager.OnBackStackChangedListener {

        @Override
        public void onBackStackChanged() {
            Fragment fragment = getFragmentManager().findFragmentByTag(_FRAGMENT_TAG);

            if (fragment instanceof KalMainFragment) {
                currentPosition = 0;
            }
            else if (fragment instanceof KalAddFragment) {
                currentPosition = 1;
            }
            else if (fragment instanceof KalOverwiewFragment) {
                currentPosition = 2;
            }
            setActionBarTitle(currentPosition);
            lvDrawer.setItemChecked(currentPosition, true);
            invalidateOptionsMenu();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(_MENU_POSITION, currentPosition);
    }


}
