package com.duzieblo.kaloryk.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Damian on 19.10.2016.
 */
public class KalorykSQLiteOpenHelper extends SQLiteOpenHelper {

    private static String _DB_NAME = "KALORYK";
    private static int _DB_VERSION = 2;

    public KalorykSQLiteOpenHelper(Context context) {
        super(context, _DB_NAME, null, _DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(getCreateTablesCommand());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            Log.d("DB_KALORYK", "update dla wersji 2");
            db.execSQL("ALTER TABLE calories ADD cal_add_date DATETIME");
        }
    }

    //TODO zrobić kolumny baz jako stałe!
    private String getCreateTablesCommand() {
        return "CREATE TABLE calories (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "cal_meal_name TEXT," +
                "cal_numb_of_calories INTEGER" +
                "cal_add_date DATETIME);";
    }



}
