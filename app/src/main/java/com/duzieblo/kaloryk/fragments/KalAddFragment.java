package com.duzieblo.kaloryk.fragments;


import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.duzieblo.kaloryk.R;
import com.duzieblo.kaloryk.helper.KalorykSQLiteOpenHelper;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class KalAddFragment extends Fragment {

    private View view;
    private Context context;

    private EditText tvMealsName;
    private EditText tvCalories;
    private Button btAddCalories;
    private TextView tvAddCalMessage;

    private Bundle savedInstanceState;

    public KalAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_kal_add, container, false);
        context = view.getContext();
        this.savedInstanceState = savedInstanceState;
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        tvMealsName     = (EditText) view.findViewById(R.id.tvMealsName);
        tvCalories      = (EditText) view.findViewById(R.id.tvCalories);
        btAddCalories   = (Button)   view.findViewById(R.id.btAddCalories);
        tvAddCalMessage = (TextView) view.findViewById(R.id.tvAddCalMessage);

        btAddCalories.setOnClickListener(getAddCaloriesOnClickListener());

        if (savedInstanceState != null) {
            tvMealsName.setText(savedInstanceState.getString("MEAL_NAME"));
            tvCalories.setText(savedInstanceState.getString("MEAL_CALORIES"));
            tvAddCalMessage.setText(savedInstanceState.getString("MESSAGE"));
        }
    }

    /**
     * Zapisuje liczbę wprowadzonych przez użytkownika kalorii do bazy
     * @return View.OnClickListener
     */
    private View.OnClickListener getAddCaloriesOnClickListener() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvMealsName.getText().toString().length() > 0 &&
                        tvCalories.getText().toString().length() > 0 ) {
                    new GetAddCaloriesTask().execute();
                } else {
                    String message = "Proszę uzupełnić nazwę posiłku i liczbę kalorii";
                    tvAddCalMessage.setText(message);
                }

            }
        };
        return onClickListener;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (tvMealsName != null && tvCalories != null && tvAddCalMessage != null) {
            String mealName = tvMealsName.getText().toString();
            String calories = tvCalories.getText().toString();
            String message  = tvAddCalMessage.getText().toString();

            outState.putString("MEAL_NAME", mealName);
            outState.putString("MEAL_CALORIES", calories);
            outState.putString("MESSAGE", message);
        }

    }

    private class GetAddCaloriesTask extends AsyncTask<Void, Void, Boolean> {

        private String mealName;
        private Integer calories;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tvAddCalMessage.setText("");
            mealName = tvMealsName.getText().toString();
            calories = Integer.parseInt(tvCalories.getText().toString());
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            KalorykSQLiteOpenHelper kalorykSQLiteOpenHelper = new KalorykSQLiteOpenHelper(context);
            try {
                SQLiteDatabase db = kalorykSQLiteOpenHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put("cal_meal_name", mealName);
                contentValues.put("cal_numb_of_calories", calories);
                contentValues.put("cal_add_date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                db.insert("calories", null, contentValues);
                return true;
            } catch (SQLException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (!aBoolean) {
                Toast.makeText(context, "Wystąpił problem z bazą danych! Skontaktuj się z twórcą " +
                        "oprogramowania", Toast.LENGTH_SHORT).show();
            } else {
                String message = "Dodałeś zjedzony posiłek o nazwie: " + mealName + ", który " +
                        "zawierał " + calories + " kcal.";
                tvAddCalMessage.setText(message);
            }
        }
    }
}
