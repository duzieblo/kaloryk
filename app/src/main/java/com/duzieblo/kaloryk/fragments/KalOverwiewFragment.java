package com.duzieblo.kaloryk.fragments;


import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.duzieblo.kaloryk.R;
import com.duzieblo.kaloryk.adapters.OverwiewCursorAdapter;
import com.duzieblo.kaloryk.helper.KalorykSQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class KalOverwiewFragment extends Fragment {

    Context context;
    View view;
    ListView lvOverwiew;

    public KalOverwiewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_kal_overwiew, container, false);
        context = inflater.getContext();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        lvOverwiew = (ListView) view.findViewById(R.id.lvOverwiew);



        try {
            KalorykSQLiteOpenHelper kalorykSQLiteOpenHelper = new KalorykSQLiteOpenHelper(context);
            SQLiteDatabase db = kalorykSQLiteOpenHelper.getReadableDatabase();
            Cursor cursor = db.query("calories",
                    new String[] {"_id", "cal_meal_name, cal_numb_of_calories, cal_add_date"},
                    "cal_add_date = ?",
                    new String[] {new SimpleDateFormat("yyyy-MM-dd").format(new Date())},
                    null,
                    null,
                    null);
            OverwiewCursorAdapter overwiewCursorAdapter = new OverwiewCursorAdapter(context, cursor);
            lvOverwiew.setAdapter(overwiewCursorAdapter);
        } catch (SQLException e) {
            Toast.makeText(context, "Wystąpił problem z bazą danych! Skontaktuj się z twórcą " +
                    "oprogramowania", Toast.LENGTH_SHORT).show();
        }

    }


}
