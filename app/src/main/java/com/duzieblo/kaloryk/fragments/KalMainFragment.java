package com.duzieblo.kaloryk.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.duzieblo.kaloryk.R;
import com.duzieblo.kaloryk.helper.KalorykSQLiteOpenHelper;

import org.w3c.dom.Text;

import java.sql.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class KalMainFragment extends Fragment {

    public interface KalMainFragmentListener {
        void onClickAddCalories();
    }

    View view;
    Context context;
    private TextView tvTotalCal;
    private Button btGoToAddCalories;

    private KalMainFragmentListener onClickCaloriesListener;


    public KalMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_kal_main, container, false);

        context = view.getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        btGoToAddCalories = (Button)view.findViewById(R.id.btGoToAddCalories);
        btGoToAddCalories.setOnClickListener(getGoToAddCalListener());


        new GetCaloriesTask().execute();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onClickCaloriesListener = (KalMainFragmentListener) activity;
    }

    private View.OnClickListener getGoToAddCalListener() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickCaloriesListener != null) {
                    onClickCaloriesListener.onClickAddCalories();
                }
            }
        };
        return onClickListener;
    }

    private class GetCaloriesTask extends AsyncTask<Void, Void, Boolean> {
        private int totalCal;
        private String errText;

        @Override
        protected Boolean doInBackground(Void... params) {
            KalorykSQLiteOpenHelper kalorykSQLiteHelper = new KalorykSQLiteOpenHelper(context);

            try {
                SQLiteDatabase db = kalorykSQLiteHelper.getReadableDatabase();

                // debugowanie!!
                /*Cursor dbCursor = db.query("calories", null, null, null, null, null, null);
                String[] columnNames = dbCursor.getColumnNames();
                for (String col: columnNames) {
                    Log.d("DB_KALORYK", col);
                }
                dbCursor.close();*/

                Cursor cursor = db.query("calories",
                                        new String[] {"sum(cal_numb_of_calories)"},
                                        "date(cal_add_date) = date()",
                                        null,
                                        null,
                                        null,
                                        null
                                        );
                if (cursor.moveToFirst()) {
                    totalCal = cursor.getInt(0);
                }
                cursor.close();
                db.close();
                errText = "";
                return true;
            } catch (SQLException e) {
                errText = e.getMessage();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                tvTotalCal = (TextView)getView().findViewById(R.id.tvTotalCal);
                tvTotalCal.setText(Integer.toString(totalCal));
            } else {
                Log.d("DB_KALORYK", errText);
                Toast.makeText(context, "Wystąpił problem z bazą danych!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
